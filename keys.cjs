function keys(testObject) {
    if (typeof (testObject) == "object") {
        let arr = []
        for (let item in testObject) {
            arr.push(`${item}`)
        }
        return arr
    }
    else {
        return []
    }
}
module.exports = keys
