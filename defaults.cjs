function defaults(testObject, defaultProps) {
    if (typeof (testObject) == "object") {
        for (let item in defaultProps) {
            if (item in testObject == false)
                testObject[item] = defaultProps[item]
        }
        return testObject
    }
}
 module.exports = defaults
