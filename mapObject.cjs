function mapObject(testObject, callback) {
    if (typeof (testObject) == "object") {
        for (let item in testObject) {
            testObject[item] = callback(testObject[item])
        }
        return testObject
    }
}
module.exports = mapObject
