function invert(testObject) {
    let revert = {}
    if (typeof (testObject) == "object") {
        for (let item in testObject) {
            revert[testObject[item]] = item
        }
        return revert
    }
}
module.exports = invert
