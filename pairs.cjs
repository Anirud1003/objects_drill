function pairs(testObject) {
    let pair = []
    for (let item in testObject) {
        let value = testObject[item]
        pair.push([item, value])
    }
    return pair
}
module.exports = pairs
