function values(testObject) {
    let array = []
    if (typeof (testObject) == "object") {
        for (let item in testObject) {
            if (typeof (testObject[item]) !== "function") {
                array.push(testObject[item])
            }
        }

    }
    return array
}
module.exports = values
